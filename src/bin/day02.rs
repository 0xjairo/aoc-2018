use docopt::Docopt;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

const USAGE: &'static str = "
Advent of Code 2018 - Day 2

Usage:
    day02 <input> [(-v | --verbose)]
    day02 (-h | --help)

Options:
    -h, --help      Show this screen
    -v, --verbose   Print verbose output
";

fn main() {
    let args = Docopt::new(USAGE)
        .and_then(|d| d.parse())
        .unwrap_or_else(|e| e.exit());

    let input = args.get_str("<input>");
    let verbose = args.get_bool("--verbose");

    println!("Part 1");
    println!("  Input: {}", input);
    let file = BufReader::new(File::open(input).unwrap());
    let ids: Vec<_> = file.lines().map(|x| x.unwrap()).collect();

    let mut letters2: u32 = 0;
    let mut letters3: u32 = 0;
    for id in &ids {
        let mut ccount = HashMap::new();
        for c in id.chars() {
            let counter = ccount.entry(c).or_insert(0);
            *counter += 1;
        }

        let mut id2 = false;
        let mut id3 = false;

        if verbose {
            println!("Word: {}", id);
        }
        for (k, v) in ccount.iter() {
            match *v {
                2 => {
                    id2 = true;
                    if verbose {
                        println!("  {} x {}", k, *v)
                    }
                }
                3 => {
                    id3 = true;
                    if verbose {
                        println!("  {} x {}", k, *v)
                    }
                }
                _ => continue,
            }
        }
        if id2 {
            letters2 += 1;
        }
        if id3 {
            letters3 += 1;
        }
    }
    println!(
        "  Num2 {}, Num3 {}, multiplied: {}",
        letters2,
        letters3,
        letters2 * letters3
    );
    println!("--");
    println!("Part 2");
    let mut mids = ids.clone();
    let mut first = "".to_string();
    let mut second = "".to_string();
    'outer: while let Some((id, rest)) = mids.split_first() {
        // vector of matches
        // attempt to match against the rest of ids,
        // iterate over the rest of the ids
        for rid in rest {
            let mut num_incorrect: u32 = 0;
            for (c, other) in id.chars().zip(rid.chars()) {
                if c != other {
                    num_incorrect += 1
                }
            }
            if num_incorrect == 1 {
                println!(
                    "  id {} does not match {} in {} place",
                    id, rid, num_incorrect
                );
                first = id.to_string();
                second = rid.to_string();
                break 'outer;
            }
        }
        mids = rest.to_vec();
    }
    print!("  Resulting string: ");
    for (f, s) in first.chars().zip(second.chars()) {
        if f == s {
            print!("{}", f);
        }
    }
    println!("");
}
