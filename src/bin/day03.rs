extern crate regex;

use docopt::Docopt;
use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

const USAGE: &'static str = "
Advent of Code 2018 - Day 3

Usage:
    day03 <input> [-v | --verbose]
    day03 (-h | --help)

Options:
    -h, --help      Show this screen
    -v, --verbose   Verbose output
";

#[derive(Debug)]
struct Point {
    x: u32,
    y: u32,
}

#[derive(Debug)]
struct Rect {
    id: u32,
    x: u32,
    y: u32,
    w: u32,
    h: u32,
}

struct Fabric {
    // width
    w: u32,
    // height
    h: u32,
    // flattened representation of each square inch of fabric where the value is the
    // number of rectangles on that given square inch
    rects: HashMap<u32, u32>,
}

fn find_fabric_size(rects: &Vec<Rect>) -> Point {
    let mut max_x = 0u32;
    let mut max_y = 0u32;
    for r in rects {
        if r.x + r.w > max_x {
            max_x = r.x + r.w;
        }
        if r.y + r.h > max_y {
            max_y = r.y + r.h;
        }
    }
    Point { x: max_x, y: max_y }
}

fn find_overlapping_area(fabric: &mut Fabric, rects: &Vec<Rect>) -> u32 {
    for r in rects.iter() {
        // for every point in this rectangle (`r`) find overlapping points
        // in other rectangles
        //println!("Rectangle {:?}", r);
        for ridx in 0..r.w * r.h {
            // get (row, column) of rectangle on fabric
            let row = (ridx / r.w) + r.y;
            let col = (ridx % r.w) + r.x;
            // flatten
            let fidx = (row * fabric.w) + col;
            //println!("ridx {}, row {}, col {}, fidx {}", ridx, row, col, fidx);
            let e = fabric.rects.entry(fidx).or_insert(0);
            *e += 1;
        }
    }

    let mut overlapping: u32 = 0;
    for (_idx, num) in &fabric.rects {
        if *num > 1 {
            overlapping += 1;
        }
    }
    overlapping
}

fn find_intact_rectangle(fabric: &Fabric, rects: &Vec<Rect>, verbose: bool) -> Option<u32> {
    for r in rects.iter() {
        let mut is_intact = true;
        for ridx in 0..r.w * r.h {
            // get (row, column) of rectangle on fabric
            let row = (ridx / r.w) + r.y;
            let col = (ridx % r.w) + r.x;
            // flattened index
            let fidx = (row * fabric.w) + col;
            // check if rectangle is not intact
            if *fabric.rects.get(&fidx).unwrap() > 1 {
                if verbose {
                    println!(
                        "#{} is not intact, first incidence at {}, {}",
                        r.id, row, col
                    );
                }
                is_intact = false;
                break;
            }
        }
        if is_intact {
            return Some(r.id);
        }
    }
    None
}

fn main() {
    let args = Docopt::new(USAGE)
        .and_then(|d| d.parse())
        .unwrap_or_else(|e| e.exit());

    let input = args.get_str("<input>");
    let verbose = args.get_bool("--verbose");

    println!("Part 3");
    println!("  Input: {}", input);
    let file = BufReader::new(File::open(input).unwrap());
    let lines: Vec<_> = file.lines().map(|x| x.unwrap()).collect();
    // regex matcher to find rectangle's x,y,h,w
    let re = Regex::new(r"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)").unwrap();
    let mut rects: Vec<Rect> = Vec::new();
    for l in lines.iter() {
        let caps = re.captures(l).unwrap();
        let r = Rect {
            id: caps.get(1).map(|m| m.as_str().parse().unwrap()).unwrap(),
            x: caps.get(2).map(|m| m.as_str().parse().unwrap()).unwrap(),
            y: caps.get(3).map(|m| m.as_str().parse().unwrap()).unwrap(),
            w: caps.get(4).map(|m| m.as_str().parse().unwrap()).unwrap(),
            h: caps.get(5).map(|m| m.as_str().parse().unwrap()).unwrap(),
        };
        rects.push(r);
    }
    let d = find_fabric_size(&rects);
    // define fabric
    let mut fabric = Fabric {
        w: d.x,
        h: d.y,
        rects: HashMap::new(),
    };
    println!("  Fabric size: (WxH) {}x{}", fabric.w, fabric.h);
    println!("  Number of rects: {}", rects.len());
    let a = find_overlapping_area(&mut fabric, &rects);
    println!("  Total overlapping area: {}", a);
    if let Some(i) = find_intact_rectangle(&fabric, &rects, verbose) {
        println!("  Intact rectangle id: {}", i);
    } else {
        println!("  No rectangle is intact");
    }
}
