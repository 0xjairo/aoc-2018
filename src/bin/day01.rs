use docopt::Docopt;
use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

const USAGE: &'static str = "
Advent of Code 2018 - Day 1

Usage:
    day01 <input>
";

fn main() {
    let args = Docopt::new(USAGE)
        .and_then(|d| d.parse())
        .unwrap_or_else(|e| e.exit());

    let input = args.get_str("<input>");

    println!("  Input: {}", input);
    let mut freqs = HashSet::new();
    let mut freq: i64 = 0;

    let fi = File::open(input).unwrap();
    let file = BufReader::new(&fi);
    let shifts: Vec<i64> = file
        .lines()
        .map(|x| x.unwrap().parse::<i64>().unwrap())
        .collect();
    let mut loops: u32 = 0;
    'outer: loop {
        for n in &shifts {
            freq += n;
            // save frequencies
            if loops > 0 && freqs.contains(&freq) {
                println!("Repeating freq: {}", freq);
                break 'outer;
            }
            freqs.insert(freq);
        }
        if loops == 0 {
            println!("  Initial resulting Frequency: {}", freq);
        }
        loops += 1;
    }
    println!("Total iteration count: {}", loops);
}
