extern crate chrono;
extern crate regex;

use docopt::Docopt;
use std::fs;
use std::io::{BufRead, BufReader};

const USAGE: &'static str = "
Advent of Code 2018 - Day 5

Usage:
    day05 <input> [-v | --verbose]
    day05 (-h | --help)

Options:
    -h, --help      Show this screen
    -v, --verbose   Verbose output
";

fn main() {
    let args = Docopt::new(USAGE)
        .and_then(|d| d.parse())
        .unwrap_or_else(|e| e.exit());

    let input = args.get_str("<input>");
    let _verbose = args.get_bool("--verbose");
    let _debug = args.get_bool("--debug");

    println!("Day 5");
    println!("  Input: {}", input);
    let file = BufReader::new(fs::File::open(input).unwrap());
    let _lines: Vec<_> = file.lines().map(|x| x.unwrap()).collect();
}
